import numpy as np
from io import StringIO

if __name__ == '__main__':
    data = u'1,2,3\n4,5,6\n'
    loaded_data = np.genfromtxt(StringIO(data), delimiter=',')
    print(loaded_data)
