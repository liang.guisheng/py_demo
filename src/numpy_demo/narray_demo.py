#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/26/19 2:18 AM
@Author: liang
@File: narray_demo.py
"""
import numpy as np


class NumpyArray:
    dimensions = None
    shape = None
    size = None
    data_type = None
    item_size = None
    data = None


def demo_1():
    a = np.arange(15).reshape(3, 5)
    assert a.shape == (3, 5)
    assert a.ndim == 2
    assert a.dtype.name == 'int64'
    assert a.itemsize == 8  # size of the item`s value in byte
    assert a.size == 15  # number of items in array
    assert isinstance(a, np.ndarray)


def array_creation_demo():
    """ Test the ways to create an array in numpy """
    simple_array = np.array([2, 3, 4])
    d2_array = np.array([1, 2, 3], [4, 5, 6])
    zero_array = np.zeros((2, 3))
    one_array = np.ones((2, 3, 4))
    empty_array = np.empty((2, 3, 4))
    range_array = np.arange(1, 20, 2)
    slice_array = np.linspace(0, 20, 9)


def basic_operation():
    a = np.array([2, 3, 4, 5])
    b = np.arange(4)
    #  normal math operator are elementwise


def index_slice_iterating():
    a = np.arange(24).reshape(2, 3, 4)
    # extract 1st element on axis 0
    c = a[0]
    c1 = a[0, :, :]
    c2 = a[0, ...]

    # extract 1st element on axis 1
    c = a[:, 0, :]

    # extract 1st element on axis 2
    c = a[:, :, 0]
    c1 = a[..., 0]


def shape_manipulate():
    a = np.arange(24).reshape((2, 3, 4))
    print(a.ravel())


def stacking_different_arrays():
    a = np.floor(10 * np.random.random((2, 2)))
    b = np.floor(10 * np.random.random((2, 2)))
    print(a, '\n', b, '\n')

    c = np.vstack((a, b))
    print(c, '\n', c.shape, '\n')

    d = np.hstack((a, b))
    print(d, '\n', d.shape, '\n')


def split_array_in_different_axis():
    a = np.floor(10 * np.random.random((4, 12)))
    for a in np.hsplit(a, 4):
        assert a.shape == (4, 3)

    a = np.floor(10 * np.random.random((4, 12)))
    for b in np.vsplit(a, 4):
        assert b.shape == (1, 12)


def copy_array():
    a = np.arange(24)
    b = a
    assert a is b  # same reference, not copy at all

    c = a.view()
    assert c is not a  # Here created a new object with the same data
    assert c.base is a
    assert not c.flags.owndata
    assert a.flags.owndata

    d = a.copy()
    assert a is not d
    assert d.base is not a


if __name__ == '__main__':
    shape_manipulate()
    stacking_different_arrays()
    split_array_in_different_axis()
    copy_array()
