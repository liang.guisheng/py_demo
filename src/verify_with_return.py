def verify_with_return():
    with open(__file__) as fp:
        return fp.read()
    print('after with return')


if __name__ == '__main__':
    verify_with_return()
