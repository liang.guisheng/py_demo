class MyIterator:
    """
    demo class to implement low level iterator protocol 
    """

    def __init__(self, step):
        self._step = step

    def __next__(self):
        print('calling next method of iterator object!')
        if self._step == 0:
            raise StopIteration()
        self._step -= 1
        return self._step

    def __iter__(self):
        print('calling __inter__ method of iterator object!')
        return self


if __name__ == "__main__":
    iter1 = MyIterator(10)
    print([o for o in iter1])
