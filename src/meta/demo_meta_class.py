def method(self):
    return 1


# The native way to create a class object
MyClass = type('People', (object,), {'method': method, 'name': 'Lucas'})


class Demo:
    def __new__(cls, *args, **kwargs):
        print('calling new')
        instance = super(Demo, cls).__new__(cls, *args, **kwargs)
        instance.db_config = {'url': 'xxx'}
        return instance

    # def __init__(self):
    #     print('calling init')


class DemoSon(Demo):

    def query(self):
        print(f'Query with configuration: {self.db_config}')


def db_config(cls):
    cls.db_config = {'url': 'yyyy'}
    return cls


@db_config
class Demo2:

    def query(self):
        print(f'Query with configuration: {self.db_config}')


class DBMeta(type):

    def __new__(cls, *args, **kwargs):
        klass = super(DBMeta, cls).__new__(cls, *args, **kwargs)
        klass.db_config = {'url': 'zzz'}
        return klass


class Demo3(metaclass=DBMeta):
    pass


class Demo4(Demo3):
    def query(self):
        print(f'Query with configuration: {self.db_config}')


if __name__ == "__main__":
    klass1 = MyClass()

    print(type(klass1))
    print(klass1.method())
    print(klass1.name)
    print(type(klass1))

    d = Demo()
    ds = DemoSon()
    ds.query()

    d2 = Demo2()
    d2.query()

    d3 = Demo4()
    d3.query()
