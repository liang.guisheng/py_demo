class MyClass:
    def __new__(cls):
        print('calling MyClass __new__ method')
        obj = super().__new__(cls)
        print(type(obj))
        return obj

    def __init__(self):
        print('calling MyClass __init__ method!')

class Meta(type):

    def __new__(cls, name, base, dct):
        sub_class = super().__new__(cls, name, base, dct)
        print('calling Meta __new__ method!')
        print(type(sub_class))
        return sub_class

class MyOtherClass(MyClass, metaclass=Meta ):
    def __init__(self):
        print('calling MyOtherClass __init__ method!')
        print(type(self))
        super().__init__()



if __name__ == "__main__":
    
    # klass = MyClass()
    # print('\n'*2)
    klass2 = MyOtherClass()

    print('\n'*2)
    print(type(MyOtherClass))


