MASTER_REALM = 'master'
REALM_NAME = 'poc'
CLIENT_ID = 'api'
CLIENT_UUID = '37028a2c-085a-40bc-b09c-743c4cec8381'
CLIENT_SECRET_KEY = 'd8b99348-8416-4bbe-9c7e-3d37659a08c4'
SERVER_URL = 'http://keycloak.poc.homolog.truckpad.com.br/auth/'
ADMIN_USERNAME = 'keycloak'
ADMIN_PASSWORD = 'keycloak'

ADMIN_ROLE = 'admin'
SUPERVISOR_ROLE = 'supervisor'
OPERATOR_ROLE = 'operator'
VIEWER_ROLE = 'viewer'

API_CLIENT_ROLES = {

    VIEWER_ROLE: {'id': 'acb22307-f0ae-42ae-8d55-e507c2aee348', 'name': 'viewer',
                  'description': "Truckpad client's viewer user", 'composite': False, 'clientRole': True,
                  'containerId': '37028a2c-085a-40bc-b09c-743c4cec8381'},

    ADMIN_ROLE: {'id': '7d1bcc67-5486-4b44-ab72-a9c94e6eff8f', 'name': 'admin', 'description': 'Truckpad admin user',
                 'composite': False, 'clientRole': True, 'containerId': '37028a2c-085a-40bc-b09c-743c4cec8381'},

    OPERATOR_ROLE: {'id': 'd4fd2477-0e98-4015-bb9d-f565d536dd8a', 'name': 'operator',
                    'description': "Truckpad client's operator user", 'composite': False, 'clientRole': True,
                    'containerId': '37028a2c-085a-40bc-b09c-743c4cec8381'},

    SUPERVISOR_ROLE: {'id': '112d40e9-8e6f-41a4-b9a1-47b086573f87', 'name': 'supervisor',
                      'description': "Truckpad client's supervisor user", 'composite': False, 'clientRole': True,
                      'containerId': '37028a2c-085a-40bc-b09c-743c4cec8381'},
}
