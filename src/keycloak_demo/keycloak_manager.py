import logging
from uuid import uuid4

from keycloak import KeycloakAdmin, KeycloakOpenID
from typing import List, Dict

from src.keycloak_demo import keycload_poc_realm_conf as conf

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class KeyCloakManager:

    def __init__(self,
                 server_url: str,
                 admin_username: str,
                 admin_password: str,
                 master_realm_name: str = 'master',
                 realm_name: str = None,
                 client_id: str = None,
                 client_secret_key: str = None
                 ):

        self.admin = KeycloakAdmin(
            server_url=server_url,
            username=admin_username,
            password=admin_password,
            realm_name=master_realm_name,
            verify=True
        )

        if realm_name:
            self.admin.realm_name = realm_name

        self.openid = KeycloakOpenID(
            server_url=server_url,
            realm_name=realm_name,
            client_id=client_id,
            client_secret_key=client_secret_key
        )

    def create_user(self,
                    external_user_id: str,
                    external_user_type: str,
                    username,
                    email,
                    password,
                    first_name: str = None,
                    last_name: str = None,
                    roles: List[str] = None,
                    office: Dict = None
                    ):
        """
        Method to create a snapshot user object of truckpad user to keycloak user

        During create a keycloak user, we can do the following actions:
            1. add global client roles to current user
            2. create user office if the given office does not exists, and then add the created user to this office
        """
        user_payload = {
            'username': username,
            'firstName': first_name,
            'lastName': last_name,
            'enabled': True,
            'email': email,
            'emailVerified': True,
            'credentials': [{'value': password, 'type': 'password'}],
            'attributes': {
                'external_user_id': [external_user_id],
                'external_user_type': [external_user_type]
            }
        }

        user = self.admin.create_user(user_payload)
        logger.info('Created new user:%s', user)

        if roles:
            client_roles = [conf.API_CLIENT_ROLES[role_name] for role_name in roles]
            logger.info('Added user role :%s', client_roles)
            self.add_user_client_roles(user_id=user, client_id=conf.CLIENT_UUID, roles=client_roles)

        if office:
            new_office = self.create_office(**office)
            self.add_user_to_office(user, new_office['id'])

        return user

    def add_user_client_roles(self, user_id: str, client_id: str = None, roles: List[Dict] = None):
        return self.admin.assign_client_role(user_id, client_id, roles)

    def create_office(self,
                      external_office_id: str,
                      name: str = None,
                      parent_id: str = None,
                      attributes: Dict = None,
                      roles: List[str] = None
                      ):

        parent = self.admin.get_group(parent_id) if parent_id else None

        office_payload = {
            'name': name,
            'attributes': {
                'external_office_id': external_office_id,
                # TODO: need more details
                'external_office_parent_id': ''
            }
        }

        return self.admin.create_group(office_payload, parent_id, False)

    def add_group_client_roles(self):
        """
        TODO: need to extend the python-keycloak lib to add client roles to office(group)
        """
        pass

    def add_user_to_office(self, user_uuid: str, office_uuid: str):
        self.admin.group_user_add(user_uuid, office_uuid)

    def get_user_token(self, email, password):
        return self.openid.token(email, password)

    def __getattr__(self, item):
        if hasattr(self.admin, item):
            return getattr(self.admin, item)
        if hasattr(self.openid, item):
            return getattr(self.openid, item)
        return None


if __name__ == '__main__':
    manager = KeyCloakManager(
        server_url=conf.SERVER_URL,
        admin_username=conf.ADMIN_USERNAME,
        admin_password=conf.ADMIN_PASSWORD,
        realm_name=conf.REALM_NAME,
        client_id=conf.CLIENT_ID,
        client_secret_key=conf.CLIENT_SECRET_KEY
    )
    new_user_payload = {
        'external_id': str(uuid4()),
        'user_type': 'shipper',
        'username': 'tp13',
        'first_name': 'tp13',
        'last_name': 'test',
        'email': 'tp13@truckpad.com.br',
        'password': 'truckpad13',
        'roles': [conf.VIEWER_ROLE, conf.OPERATOR_ROLE],
        'office': {
            'name': 'Bahia2',
            'roles': [conf.ADMIN_ROLE],
            'parent_id': '83011a03-bbcb-413c-921e-3b0164d845e5',
            'attributes': {
                'external_office': [{'id': str(uuid4()), 'name': 'Salvador'}]
            }
        }
    }
    # user = manager.create_user(**new_user_payload)
    # manager.add_user_to_office('8c7b3cfb-dab2-4660-b48d-9b9baca320f0', '50e5e3fb-3758-4a2a-b3c5-85e7c30b30eb')
    #
    token = manager.get_user_token(new_user_payload['email'], new_user_payload['password'])
    print(token['access_token'])

    new_office_payload = {
        'name': 'Pernambuco',
        'parent_id': '83011a03-bbcb-413c-921e-3b0164d845e5',
        'roles': [conf.ADMIN_ROLE],
        'attributes': {
            'external_office': [{'id': str(uuid4()), 'name': 'Recife'}]
        }
    }

    # office = manager.create_office(**new_office_payload)
    # print(f'Created new office:\n{office}')
    # office = manager.admin.get_group('2054ce87-2823-44a1-9287-69c76f482acf')
    # print(office)
