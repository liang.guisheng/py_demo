import json
from keycloak import KeycloakOpenID, KeycloakAdmin

keycloak_openid = KeycloakOpenID(server_url='http://localhost:8080/auth/', 
                                client_id='truckpad', 
                                realm_name='test',
                                client_secret_key='d718885a-f0b9-4a28-a019-0c7a9fe46ad8'
                                )

config_well_know = keycloak_openid.well_know()
token = keycloak_openid.token(username='liang', password='test123', grant_type='password')
print(token)
print()

userinfo = keycloak_openid.userinfo(token['access_token'])
print(userinfo)


print(keycloak_openid.introspect(token['access_token']))



admin = KeycloakAdmin(server_url='http://localhost:8080/auth/', username='liang', password='test123', realm_name='test', verify=True)


all_groups = admin.get_groups()

print('\n\n')

print(json.dumps(all_groups, indent=1))

# group = admin.create_group(payload={'name':'query'},parent='f2fd6b76-dbf1-4600-bc5f-22a2a2a7d325')
# print('\n'*2, 'created group:', group)


