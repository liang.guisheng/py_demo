#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/14/19 8:26 PM
@Author: liang
@File: websocket_demo.py
"""
from unittest.mock import MagicMock

from ws4py.messaging import TextMessage
from ws4py.websocket import EchoWebSocket


def data_source():
    yield TextMessage(u'Hello world!')


source = MagicMock(side_effect=data_source)

ws = EchoWebSocket(sock=source)

if __name__ == '__main__':
    ws.send(u'Hello there')
