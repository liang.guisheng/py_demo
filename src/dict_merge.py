import json
from copy import deepcopy
from enum import Enum
from typing import Type, Optional, Tuple, Union, Dict, Any, List


class ItemMergeType(Enum):
    # for non-dict item
    LIST_EXTEND = 1  # add new list items into old list
    LIST_SET = 2  # add new list items into old only when new items does not exist in old list

    # for dict items
    DICT_UPDATE = 3  # if item exist use new to update old

    # for both dict & non-dict item
    REPLACE = 4  # use new list value to replace old list directly

    IGNORE = 5  # ignore the incoming field's value

    UPSERT = 6


class ItemValueType(Enum):
    DICT = 1
    LIST = 2
    SCALAR = 3


class MergeOption:

    def __init__(self, data_type: ItemValueType = None, override: bool = None, allow_null: bool = True,
                 item_merge_type: ItemMergeType = ItemMergeType.LIST_SET,
                 index: Optional[Tuple] = None,
                 ignore_fields: Optional[Tuple] = None
                 ):
        self.data_type = data_type
        self.override = override
        self.allow_null = allow_null
        self.item_merge_type = item_merge_type
        self.index = index
        self.ignore_fields = None

    def __str__(self):
        return f'''data_type={self.data_type}, override={self.override}, allow_null={self.allow_null}, 
        item_merge_type={self.item_merge_type}, index={self.index}'''


class ListMergeOption(MergeOption):
    """
    List value field default merge all items both in new and old data object and keep unique item

    For List which only includes dict value object, can specify index for dict object to identify unique
    """

    def __init__(self, index: Optional[Tuple] = None, item_merge_type: ItemMergeType = ItemMergeType.LIST_SET):
        super(ListMergeOption, self).__init__(data_type=ItemValueType.LIST, index=index,
                                              item_merge_type=item_merge_type)


class DictMergeOption(MergeOption):
    """
    Dict value field default merge the new dict object into old dict object directly
    """

    def __init__(self, item_merge_type: ItemMergeType = ItemMergeType.DICT_UPDATE,
                 ignore_fields: Optional[Tuple] = None):
        super(DictMergeOption, self).__init__(data_type=ItemValueType.DICT, item_merge_type=item_merge_type,
                                              ignore_fields=ignore_fields)


class ScalarMergeOption(MergeOption):
    """
    Scalar value field default merge type is use new value to replace old value
    """

    def __init__(self, item_merge_type: ItemMergeType = ItemMergeType.REPLACE):
        super(ScalarMergeOption, self).__init__(data_type=ItemValueType.SCALAR, item_merge_type=item_merge_type)


class MergePolicy:

    def __init__(self, policy: Dict):
        self.policy = policy or {}

    @property
    def dict(self):
        return self.policy

    @dict.setter
    def dict(self, value: Dict):
        self.policy = value


class DictMerger:
    origin_data = None
    input_data = None
    merged = None
    change_logs = None

    def __init__(self, old_dict_data, new_dict_data):
        self.origin_data = old_dict_data
        self.input_data = new_dict_data

    def merge(self, policy: Union[MergePolicy, Dict] = None) -> (Dict, Dict):
        """
        merge new data to old data with the given policy
        """
        policy = policy.dict if isinstance(policy, MergePolicy) else policy or {}
        self.merged = deepcopy(self.origin_data)
        self.change_logs = {}
        self._merge('', self.merged, deepcopy(self.input_data), policy)
        return self.merged, self.change_logs

    def _merge(self, key_prefix: str, old_data: Dict, new_data: Dict, policy: Dict):

        # record the changes between the 2 data objects
        old_keys, new_keys = old_data.keys(), new_data.keys()
        added_new_keys = new_keys - old_keys
        removed_keys = old_keys - new_keys

        self.change_logs.setdefault('add_new_keys', []).extend(
            [{f'{key_prefix}.{k}' if key_prefix else k: new_data[k]} for k in added_new_keys])
        self.change_logs.setdefault('removed_keys', []).extend(
            [{f'{key_prefix}.{k}' if key_prefix else k: old_data[k]} for k in removed_keys])

        for k, v in new_data.items():
            complete_key_name = f'{key_prefix}.{k}' if key_prefix else k

            if isinstance(v, list):
                merge_option = policy.get(complete_key_name, ListMergeOption())
                old_data[k] = self._merge_list_field(k, old_data.get(k) or [], new_data.get(k) or [], merge_option)
            elif isinstance(v, dict) and k in old_data and isinstance(old_data[k], dict):
                self._merge(complete_key_name, old_data.get(k), new_data.get(k), policy)
            else:
                merge_option = policy.get(complete_key_name, ScalarMergeOption())
                if merge_option.item_merge_type == ItemMergeType.IGNORE:
                    continue
                old_data[k] = self._merge_scalar_field(k, old_data.get(k), new_data.get(k), merge_option)

    def _merge_list_field(self, key: str, old_data: List, new_data: List, option: MergeOption) -> Any:
        if option.index:
            return self._merge_dict_item_list_field(key, old_data, new_data, option)
        if option.item_merge_type:
            return self._merge_plain_item_list_field(key, old_data, new_data, option)
        raise Exception('List field should specify either index or list_item_merge_type attribute')

    @staticmethod
    def _merge_dict_item_list_field(key: str, old_data: List, new_data: List, option: MergeOption) -> Any:
        """
        This method to handle a list which contains only dict object items
        """
        if option.item_merge_type == ItemMergeType.REPLACE:
            return old_data

        item_index_keys = option.index
        old_data_key_index = {}
        new_data_key_index = {}

        for idx, item in enumerate(old_data):
            item_key = '-'.join([str(item.get(k)) for k in item_index_keys])
            old_data_key_index[item_key] = idx

        for idx, item in enumerate(new_data):
            item_key = '-'.join([str(item.get(k)) for k in item_index_keys])
            new_data_key_index[item_key] = idx

        for index_key, item_index in old_data_key_index.items():
            new_item_index = new_data_key_index.get(index_key)
            if new_item_index is None:
                continue
            # update old dict item with new dict item excluding index fields
            if option.item_merge_type == ItemMergeType.DICT_UPDATE:
                updated_value = {k: v for k, v in new_data[new_item_index].items() if k not in option.index}
                old_data[item_index].update(updated_value)
            new_data[new_item_index] = None
        old_data.extend([i for i in new_data if i])
        return old_data

    @staticmethod
    def _merge_plain_item_list_field(key: str, old_data: List, new_data: List, option: MergeOption) -> Any:
        """
        This method is used to handle list field which its items are not dict object
        """
        if option.item_merge_type == ItemMergeType.LIST_EXTEND:
            return old_data + new_data
        if option.item_merge_type == ItemMergeType.LIST_SET:
            return list(set(old_data + new_data))
        if option.item_merge_type == ItemMergeType.REPLACE:
            return new_data
        if option.item_merge_type.IGNORE:
            return old_data
        raise Exception(f'Invalid item merge type value:{option.item_merge_type}')

    @staticmethod
    def _merge_scalar_field(key: str, old_data: Dict, new_data: Dict, option: MergeOption) -> Any:
        """
        This method will handle a field as a scalar value, ignore its real data type
        """
        if option.item_merge_type == ItemMergeType.IGNORE:
            return old_data
        return new_data


if __name__ == '__main__':
    origin_data = {
        'id': '1',
        'info': {
            'name': 'test',
            'age': 12,
            'main_contact': {
                'country_code': 55,
                'are_code': 12,
                'number': '996442508'
            },
            'documents': [
                {'id': 123,
                 'doc_type': 'cpf',
                 'number': 2345},
                {'doc_type': 'rg',
                 'number': 456789}
            ],
            'company': {
                'name': 'tp',
                'contacts': [
                    {
                        'contact_type': 'email',
                        'address': 'test@a.com'
                    },
                    {
                        'contact_type': 'cell',
                        'number': '998769087'
                    }
                ]
            }
        },
        'fruits': ['apple', 123, 345, True],
        'tracker': None
    }

    input_data = {
        'new': 'haha',
        'fruits': ['pear', 'haha'],
        'tracker': {
            'name': 'sascer'
        },
        'info': {
            'documents': [
                {
                    'doc_type': 'cpf',
                    'number': '2345',
                    'name': 'liang'
                },
                {
                    'doc_type': 'rg',
                    'number': 456789
                },
                {
                    'doc_type': 'cnh',
                    'number': 123412312
                }
            ],
            'company': {
                'name': 'tp',
                'contacts': [
                    {
                        'contact_type': 'email',
                        'address': 'test@a.com',
                        'name': 'liang'
                    },
                    {
                        'contact_type': 'cell',
                        'number': '998769089'
                    }
                ]
            }
        }
    }

    merger = DictMerger(origin_data, input_data)
    policy1 = {
        'info.documents': ListMergeOption(index=('doc_type', 'number'), item_merge_type=ItemMergeType.DICT_UPDATE),
        'info.company.contacts': ListMergeOption(index=('contact_type', 'number', 'address'),
                                                 item_merge_type=ItemMergeType.DICT_UPDATE),
        'fruits': ListMergeOption(item_merge_type=ItemMergeType.REPLACE),
        'new': ScalarMergeOption(item_merge_type=ItemMergeType.REPLACE),
        'tracker': ScalarMergeOption(item_merge_type=ItemMergeType.REPLACE)
    }

    result, log = merger.merge(policy1)
    policy2 = MergePolicy(policy1)
    result2, log2 = merger.merge(policy2)
    print(result == result2)
    print(json.dumps(result, indent=2))
