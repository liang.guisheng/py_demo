#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/14/19 8:41 PM
@Author: liang
@File: ws_server_demo.py
"""


def wsgiref_server():
    from wsgiref.simple_server import make_server
    from ws4py.server.wsgirefserver import WebSocketWSGIRequestHandler, WebSocketWSGIHandler, WSGIServer
    from ws4py.server.wsgiutils import WebSocketWSGIApplication
    from ws4py.websocket import EchoWebSocket

    class MyEchoWebSocket(EchoWebSocket):

        def opened(self):
            print(f'Openning connection')

        def closed(self, code, reason=None):
            print(f'Closing connection: {code}, {reason}')

        def received_message(self, message):
            print(f'Received client message:{message}')
            return super(MyEchoWebSocket, self).received_message(message)

    server = make_server('', 9000, server_class=WSGIServer,
                         handler_class=WebSocketWSGIRequestHandler,
                         app=WebSocketWSGIApplication(handler_cls=MyEchoWebSocket)
                         )

    server.initialize_websockets_manager()
    server.serve_forever()


if __name__ == '__main__':
    wsgiref_server()
