import asyncio
import websockets
from websockets import WebSocketServerProtocol, WebSocketServer

USERS = {}


async def hello(websocket: WebSocketServerProtocol, path):
    USERS.setdefault(path, []).append(websocket)
    print(type(websocket))
    while True:
        print(f'accessing {path}')

        name = await  websocket.recv()
        print(f'< {name}')

        greeting = f'Hello {name}!'
        for ws in USERS[path]:
            await ws.send(greeting)
        await websocket.send(greeting)

        print(f'> {greeting}')

        for ws in USERS[path]:
            print(getattr(ws, 'request_headers', {}))


class MyWSServer(WebSocketServerProtocol):

    def __init__(self, ws_handler=hello, ws_server=WebSocketServer, *args, **kwargs):
        super(MyWSServer, self).__init__(ws_handler=ws_handler, ws_server=ws_server, *args, **kwargs)


start_server = websockets.serve(hello, 'localhost', 8765)

asyncio.get_event_loop().run_until_complete(start_server)

asyncio.get_event_loop().run_forever()
