class APIDoc:
    def __get__(self, instance, owner):
        obj = instance or owner
        return {o: getattr(v, '__doc__').strip() for o, v in obj.__dict__.items() if
                not o.startswith('_') and callable(v)}


def inspect_class_api_doc(cls):
    cls.__apidoc__ = APIDoc()

    return cls


@inspect_class_api_doc
class Demo:
    name = 'Celso'

    def __init__(self, name=None):
        self.name = name or self.name

    def hello(self, name=None):
        """
        one demo hello method call
        """
        print(f'Hello, {name or self.name}')


@inspect_class_api_doc
class Demo2:
    number = '123'

    def dial(self):
        """
        dialing a number
        """
        print('dialing ' + self.number)


if __name__ == '__main__':
    print(Demo.__apidoc__)
    print(Demo2.__apidoc__)
