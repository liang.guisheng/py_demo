from pydantic import BaseModel
from typing import List


class Model(BaseModel):
    a: List[int]


class Foo(BaseModel):
    a: int = 123
    sibling: 'Foo' = None


Foo.update_forward_refs()

print(Model(a=('1', 2, 3, 3)))

print(Foo())
