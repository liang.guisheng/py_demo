from pydantic import BaseModel


class Item(BaseModel):
    name: str
    price: float
    quantity: int

    class Config:
        # used to customize the client input
        alias_generator = lambda field_name: ''.join([word.capitalize() for word in field_name.split('_')])


item = Item(Name='Coco', Price=5.0, Quantity=10)
print(item)
print(item.name, item.price, item.quantity)
print(item.schema(by_alias=True))
