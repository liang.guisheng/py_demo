from datetime import datetime

from pydantic.dataclasses import dataclass
from pydantic.networks import AnyUrl


@dataclass
class User:
    id: int
    name: str = 'John Doe'
    signup_ts: datetime = None


user = User(id='42', signup_ts='2020-02-02 12:30')
print(user)


@dataclass
class NavbarButton:
    href: AnyUrl


@dataclass
class Navbar:
    button: NavbarButton


navbar = Navbar(button=('https://google.com',))

print(navbar)


@dataclass
class Birth:
    year: int
    month: int
    day: int


@dataclass
class User2:
    birth: Birth

    def __post_init__(self):
        print(f'post init:{self.birth}')

    def __post_init_post_parse__(self):
        print(f'post init post parse:{self.birth}')


user = User2(**{'birth': {'year': 2000, 'month': 2, 'day': 12}})


