from pydantic import BaseSettings, Field
from typing import Set


class Settings(BaseSettings):
    cluster: str = Field('stage', env='CLUSTER')  # specified variable name for setting field, has high priority
    api_env: str = Field('dev')  # user customized variable name setting override by the env_prefix in Config
    auth_key: str = None  # customized variable name setting in fields in Config class
    # list environment variable should be exported as json-encoded string
    domains: Set[str] = set()

    class Config:
        # customize environment variable config name prefix
        env_prefix = 'my_prefix_'

        # customized field environment variable name
        fields = {
            'auth_key': {
                'env': 'AUTH_KEY'
            }
        }

        case_sensitive = True


print(Settings().dict())


class EnvSettings(BaseSettings):
    api_env: str
    redis_address: str
    meaning_of_life: int
    my_var: str

    class Config:
        env_file = '.env'


# use Config subclass to extract default .env config file
print(EnvSettings().dict())

# Class init parameter has higher priority the default Config subclass
print(EnvSettings(_env_file='staging.env').dict())
