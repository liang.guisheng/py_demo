from datetime import datetime
from random import randint
from typing import List

from pydantic import BaseModel, ValidationError, root_validator, validator


class User(BaseModel):
    id: int
    name = 'John Doe'
    signup_ts: datetime = None
    friends: List[int] = []

    @root_validator(pre=True)  # pre_load corresponding
    def check_user_id(cls, values):
        if 'id' not in values:
            values['id'] = randint(1, 100)
        return values

    @validator('signup_ts', pre=True, always=True)
    def set_signup_ts_now(cls, v):
        return v or datetime.now()

    @validator('friends', pre=True, each_item=True)
    def check_friends(cls, v):
        assert str(v).isdigit(), f'{v} is not a digit value!'
        return v


client_data = {
    'friends': [1, 2, 3, '4', '5']
}

user = User(**client_data)
print(user)
print(user.schema())

