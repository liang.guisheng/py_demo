import pickle
from enum import Enum

from pydantic import BaseModel, Field


class FooBar(BaseModel):
    count: int
    size: float = None


class Gender(str, Enum):
    male = 'male'
    female = 'female'
    other = 'other'
    not_given = 'not_given'


class MainModel(BaseModel):
    foo_bar: FooBar = Field(...)
    gender: Gender = Field(None, alias='Gender')
    snap: int = Field(42, title='The snap', description='This is the value of snap', gt=30, lt=50)

    class Config:
        title = 'Main'


# print(MainModel.schema_json(indent=2))

model = MainModel(**{'foo_bar': {'count': 10, 'size': 12.6}, 'Gender': Gender.male})
print(model.dict())
print(model.dict(by_alias=True, exclude_defaults=False))
print(model.dict(by_alias=True, exclude_unset=True))
print(model.dict(by_alias=True, exclude_defaults=True))

print(dict(model))

print(model.copy())

print(model.json())

pickle_dumped_model = pickle.dumps(model)
print(pickle_dumped_model)
pickle_loaded_model = pickle.loads(pickle_dumped_model)
print(pickle_loaded_model)
