from flask import Flask, url_for, render_template, request

app = Flask(__name__)


@app.route('/')
def index():
    return 'Index page!'


@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)


# variable rules
@app.route('/user/<username>')
def show_user_profile(username):
    return f'User {username}!'


@app.route('/post/<int:post_id>')
def show_post(post_id):
    return f'Post {post_id}!'


@app.route('/path/<path:subpath>')
def show_subpath(subpath):
    return f'Subpath {subpath}'


# unique urls/redirection behavior
@app.route('/projects/')
def projects():
    return 'The project page'


@app.route('/about')
def about():
    print(url_for('about', next='/projects'))
    return 'The about page'


# http method for view
@app.route('/method', methods=['POST'])
def method():
    return 'Request method test!'


# access request object dat
@app.route('/request_inspect')
def request_inspect():
    print(
        request.environ, '\n',
        request.method, '\n',
        request.path, '\n',
        request.full_path, '\n',
        request.values, '\n',
        request.headers, '\n',
        request.query_string, '\n',
        request.remote_addr, '\n',
        request.form,'\n',
        request.args, '\n',
        request.files, '\n',
        request.cookies, '\n',

    )
    return 'Inspecting http request'
