import threading


class Request:

    def __init__(self, data: dict = None):
        self.data = data or {}


def local_property():
    ls = threading.local()

    def fget(self):
        try:
            print('accessing ')
            return ls.var
        except AttributeError:
            raise RuntimeError('Context not initialized')

    def fset(self, value):
        print(f'setting: {value}!')
        ls.var = value

    def fdel(self):
        print('deleting')
        del ls.var

    return property(fget, fset, fdel, 'thread local property')


class LocalRequest(Request):
    bind = Request.__init__
    data = local_property()


request = LocalRequest()
request.bind({'name': 'request'})

