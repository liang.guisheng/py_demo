def consumer():
    r = ''
    while True:
        v = yield r
        print('[Consumer] Receiving %s' % v)
        v *= 2
        r = v


def producer(c):
    c.send(None)
    for i in list(range(1, 6)):
        print('[Producer] producing %s...' % i)
        result = c.send(i)
        print('[Producer] consumer return: %s' % result)
    c.close()


c = consumer()
producer(c)
