#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/14/19 7:28 PM
@Author: liang
@File: rq_demo.py
"""
import time

from redis import Redis
from rq import Queue

from src.rq_util import count_words_at_url

if __name__ == '__main__':
    q = Queue(connection=Redis(), is_async=True)
    jobs = []
    for i in range(10):
        job = q.enqueue(count_words_at_url, 'http://www.google.com')
        print(job.result)
        # time.sleep(2)
        print(job.result)
