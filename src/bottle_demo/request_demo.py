from bottle import Bottle, request, response

app = Bottle()


@app.get('/hello')
def hello():
    response.content_type='application/json'
    return {'message': 'Hello, world!'}


if __name__ == '__main__':
    app.run(port=9009, debug=True, reloader=True)
