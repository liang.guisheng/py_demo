from concurrent.futures import ThreadPoolExecutor

import requests
from pyquery import PyQuery as pq
from typing import List

MAX_LEVEL = 10
PROCESSED_LINKS = {}


def init_page_links(urls: List[str]):
    for url in urls:
        base_url_path = url.rpartition('/')[0] + '/'
        host = url[0:url.index('/', 8)]
        doc = pq(url=url)
        links = doc('a')
        for link in links:
            link_elem = pq(link)
            sub_link: str = link_elem.attr('href')
            if sub_link.startswith('#'):
                sub_link = url + sub_link
            elif not sub_link.startswith('http'):
                if sub_link.startswith('/'):
                    sub_link = host + sub_link
                else:
                    sub_link = base_url_path + sub_link
            walk_sub_link(sub_link, 1)


def walk_sub_link(url: str, level: int = 1):
    if not url or url in PROCESSED_LINKS or level > MAX_LEVEL:
        return
    PROCESSED_LINKS[url] = 1
    print(f'Processed: {len(PROCESSED_LINKS)}, Processing url: {url}!')
    base_url_path = url.rpartition('/')[0] + '/'
    host = url[0:url.index('/', 8)]
    try:
        doc = pq(url=url)
        for link in doc('a'):
            sub_url: str = pq(link).attr('href')
            if sub_url.startswith('#'):
                sub_url = url.partition('#')[0] + sub_url
            elif not sub_url.startswith('http'):
                if sub_url.startswith('/'):
                    sub_url = host + sub_url
                else:
                    sub_url = base_url_path + sub_url
            walk_sub_link(sub_url, level + 1)
    except:
        print(f'error:{url}')


if __name__ == '__main__':
    init_urls = [
        'https://pythonhosted.org/pyquery/manipulating.html',
        'https://v.qq.com/',
        'https://www.youku.com/',
        'https://www.youtube.com/',
        'https://ctolib.com/',
        'https://linux.cn/'
    ]
    # init_page_links(init_urls)
    with ThreadPoolExecutor(max_workers=20) as tpe:
        future = tpe.map(walk_sub_link, init_urls)

    # with ThreadPoolExecutor(max_workers=20) as tpe:
    #     block = []
    #     for url in PROCESSED_LINKS:
    #         block.append(url)
    #         if len(block) > 200:
    #             tpe.map(lambda o: requests.get(o), block[:])
    #             block = []
    #     if block:
    #         tpe.map(lambda o: requests.get(o), block)

    print(PROCESSED_LINKS)
