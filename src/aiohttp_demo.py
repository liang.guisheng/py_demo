#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/11/19 3:13 PM
@Author: liang
@File: aiohttp_demo.py
"""
from aiohttp import web


async def hello(request):
    return web.Response(text='Hello world')


############################################################
# Application
############################################################

app = web.Application(debug=True)
app.add_routes([web.get('/', hello)])

if __name__ == '__main__':
    web.run_app(app, port=8010)
