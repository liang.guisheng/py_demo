#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/14/19 7:31 PM
@Author: liang
@File: rq_util.py
"""
import logging

import requests

logger = logging.getLogger(__name__)


def count_words_at_url(url):
    response = requests.get(url)
    logger.info('Fetch content of %s!\n', url)
    return len(response.text.split())
