from enum import IntEnum

from typing import Optional, Tuple


class FieldMergeOption(IntEnum):
    ARRAY_EXTEND = 1
    ARRAY_SET = 2
    ARRAY_REPLACE = 3

    DICT_EXTEND = 4
    DICT_REPLACE = 5

    SCALAR_REPLACE = 6
    IGNORE = 7


class FieldValueType(IntEnum):
    DICT = 1
    ARRAY = 2
    SCALAR = 3


class FieldMergePolicy:

    def __init__(self,
                 field_type: FieldValueType = None,
                 field_value_merge_option: FieldMergeOption = None,
                 index: Optional[Tuple] = None
                 ):
        self.field_type = field_type
        self.field_value_merge_option = field_value_merge_option
        self.index = index


class ArrayFieldMergePolicy(FieldMergePolicy):
    def __init__(self, field_value_merge_option: FieldMergeOption = None, index: Optional[Tuple] = None):
        super(ArrayFieldMergePolicy, self).__init__(field_type=FieldValueType.ARRAY,
                                                    field_value_merge_option=field_value_merge_option, index=index)


class DictFieldMergePolicy(FieldMergePolicy):
    def __init__(self, field_value_merge_option: FieldMergeOption = None):
        super(DictFieldMergePolicy, self).__init__(field_type=FieldValueType.DICT,
                                                   field_value_merge_option=field_value_merge_option)


class ScalarFieldMergePolicy(FieldMergePolicy):
    def __init__(self, field_value_merge_option: FieldMergeOption = None):
        super(ScalarFieldMergePolicy, self).__init__(field_type=FieldValueType.SCALAR,
                                                     field_value_merge_option=field_value_merge_option)
