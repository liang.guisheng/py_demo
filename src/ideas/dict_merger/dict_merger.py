from copy import deepcopy
from typing import Dict


class DictMerger:

    def __init__(self, origin_data: Dict = None, input_data: Dict = None):
        self.origin_data = origin_data
        self.input_data = input_data
        self.merged = None
        self.changed = None

    def merge(self, policy: Dict = None) -> (Dict, Dict):
        """
        merge the input data to the origin data by the given merge policy
        @:parameter
            policy: specify how the merge to merge the 2 collection data
        @:return:
            merged data
            change logs
        """
        self.merged = deepcopy(self.origin_data)
        self.changed = {}

    def _merge(self, key_prefix: str, origin: Dict, input: Dict, policy: Dict):
        pass
