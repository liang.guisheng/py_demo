import asyncio


async def example():
    await asyncio.sleep(1)
    print('print in main thread')


coro = example()

loop = asyncio.get_event_loop()
task = loop.create_task(coro)
print('check 1', task)

loop.run_until_complete(task)
print('check 2', task)
loop.close()
