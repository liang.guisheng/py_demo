# import asyncio, time
# 
# def now():
#     return time.time()
# 
# async def do_some_work(x):
#     print('Waiting {}'.format(x))
#     return 'Done after {}s'.format(x)
# 
# start = now()
# 
# coroutine = do_some_work(2)
# loop = asyncio.get_event_loop()
# task = asyncio.ensure_future(coroutine)
# loop.run_until_complete(task)
# 
# print('Task ret: {}'.format(task.result()))
# print('TIME: {}'.format(now() - start))


import asyncio

import time

now = lambda: time.time()

async def do_some_work(x, sleep=1):
    print('Waiting: ', x)

    await asyncio.sleep(sleep)
    return 'Done after {}s'.format(x)

start = now()

coroutine1 = do_some_work(1, 1)
coroutine2 = do_some_work(2,2)
coroutine3 = do_some_work(3, 3)
coroutine4 = do_some_work(4, 4)
coroutine5 = do_some_work(5, 5)
coroutine6 = do_some_work(6,6)
coroutine7 = do_some_work(7,7)
coroutine8 = do_some_work(8,8)
coroutine9 = do_some_work(9, 9)

tasks = [
    asyncio.ensure_future(coroutine1),
    asyncio.ensure_future(coroutine2),
    asyncio.ensure_future(coroutine3),
    asyncio.ensure_future(coroutine4),
    asyncio.ensure_future(coroutine5),
    asyncio.ensure_future(coroutine6),
    asyncio.ensure_future(coroutine7),
    asyncio.ensure_future(coroutine8),
    asyncio.ensure_future(coroutine9)
]

loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.wait(tasks))

for task in tasks:
    print('Task ret: ', task.result())

print('TIME: ', now() - start)
