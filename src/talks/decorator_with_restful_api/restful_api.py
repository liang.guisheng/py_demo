import time
import types

ACCESS_ROLES_KEY = 'access_roles'
IS_PUBLIC_KEY = 'is_public'

# Global API application object
application = {}


# Function decorator
def register_endpoint(methods=None, url='', obj=None, name=None):
    if not url:
        raise Exception('url is required!')
    methods = methods or ['GET']
    name = name or obj.__name__
    application.setdefault(url, {})
    for method in methods:
        application[url][method.upper()] = {'callback': obj, 'name': name}
    return obj


# Decorator endpoint decorator
def register(methods=None, url='', name=None):
    methods = methods or ['GET']

    def wrapper(obj):
        return register_endpoint(methods, url, obj, name)

    return wrapper


# Simple decorator to mark public endpoint
def is_public(obj):
    setattr(obj, IS_PUBLIC_KEY, True)
    return obj


def only_admin_role(obj):  # mark endpoint only accessible by admin
    setattr(obj, ACCESS_ROLES_KEY, ['admin'])
    return obj


def only_system_role(obj):  # mark endpoint only accessible by system
    setattr(obj, ACCESS_ROLES_KEY, ['system'])
    return obj


def access_role(roles: list = None):  # mark endpoint accessible only by the specified roles
    roles = roles or []

    def role_wrapper(obj):
        setattr(obj, ACCESS_ROLES_KEY, roles)
        return obj

    return role_wrapper


def timer(label='', tracer=True):
    def timer_decorator(func):
        calls = 0
        all_times = 0

        def wrapper(*args, **kwargs):
            nonlocal calls, all_times
            calls += 1
            start = time.clock()
            result = func(*args, **kwargs)
            elapse = time.clock() - start
            all_times += elapse
            if tracer:
                print(f'{label}: calling {calls} to {func.__name__}: {elapse}, {all_times}')
            return result

        return wrapper

    return timer_decorator


# Helper method to mock http request
def request(method='GET', url='', name='', key=None, roles: list = None):
    roles = roles or []
    for api_url, route in application.items():
        if api_url == url:

            callback = route[method.upper()]['callback'] if method.upper() in route else None
            if not callback:
                return f'Method: {method} is not allowed!'

            if getattr(callback, ACCESS_ROLES_KEY, []) and not set(getattr(callback, ACCESS_ROLES_KEY, [])) & set(
                    roles):
                return f'Protected endpoint: {route[method.upper()]["name"]} can not accessed by {roles}!'

            # client with corresponding access role can access protect endpoint
            if not getattr(callback, ACCESS_ROLES_KEY, []) and not key and not getattr(callback, IS_PUBLIC_KEY, False):
                return f'Protect endpoint: {route[method.upper()]["name"]} can not access without key'

            return callback()

    return 'Page not found'
