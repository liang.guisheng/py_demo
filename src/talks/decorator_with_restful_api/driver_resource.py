from src.talks.decorator_with_restful_api.restful_api import register_endpoint, request, register, only_admin_role, \
    only_system_role, access_role, timer, is_public


##################################################
@is_public
def get_list():
    return 'get drivers list'


register_endpoint(['GET'], '/drivers', get_list, 'driver_list')


##################################################


@register(['GET'], '/driver/123', 'driver_object')
@only_admin_role
def get_object():
    return 'get driver object'


@register(['POST'], '/drivers', 'create_drivers')
@only_system_role
def post_list():
    return 'created multiple drivers'


@register(['POST'], '/drivers/123', 'create one driver')
@access_role(['admin', 'system', 'operator'])
@timer(label='Endpoint>>>>')
def post_object():
    return 'created driver object'


def put_list():
    return 'updated multiple drivers'


def put_object():
    return 'updated driver object'


def patch_list():
    return 'updated multiple drivers'


def patch_object():
    return 'updated driver object'


def delete_list():
    return 'updated multiple drivers'


def delete_object():
    return 'updated driver object'


if __name__ == '__main__':
    result = request(url='/drivers')
    print(result)

    result = request(url='/driver/123', roles=['admin'])
    print(result)

    result = request(method='post', url='/drivers/123', roles=['operator'])
    print(result)
