import logging

import uvicorn
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.requests import Request
from starlette.responses import PlainTextResponse
from starlette.routing import Route, WebSocketRoute, Mount
from starlette.staticfiles import StaticFiles
from starlette.types import ASGIApp, Scope, Receive, Send


def homepage(request):
    print(request.app.state.ADMIN_EMAIL)
    return PlainTextResponse('Hello, world!')


def user_me(request):
    print(request.app.state.ADMIN_EMAIL)
    username = 'John Doe'
    return PlainTextResponse('Hello, {}!'.format(username))


def user(request: Request):
    username = request.path_params['username']
    if username == 'error':
        raise Exception('server error happened!')
    return PlainTextResponse('Hello, {}!'.format(username))


async def websocket_endpoint(websocket):
    await websocket.accept()
    await websocket.send_text('Hello, websocket!')
    await websocket.close()


def startup():
    print('Ready to go!')


def shutdown():
    print('I am done!')


routes = [
    Route('/', homepage),
    Route('/user/me', user_me),
    Route('/user/{username}', user),
    WebSocketRoute('/ws', websocket_endpoint),
    Mount('/static', StaticFiles(directory='static'))

]


class APMMiddleware:
    def __init__(self, app: ASGIApp) -> None:
        self.app = app
        self.logger = logging.getLogger(__file__)

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        self.logger.info('starting apm logger')
        await self.app(scope, receive, send)
        self.logger.info('finishing apm logger')


app = Starlette(
    debug=True,
    routes=routes,
    middleware=[Middleware(APMMiddleware)],  # how to write starlette middleware
    exception_handlers={},  # how to write starlette exception handlers
    on_startup=[startup],
    on_shutdown=[shutdown]
)

app.state.ADMIN_EMAIL = 'admin@example.org'

if __name__ == '__main__':
    uvicorn.run(app=app)
