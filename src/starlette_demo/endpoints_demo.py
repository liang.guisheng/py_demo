import uvicorn
from starlette.applications import Starlette
from starlette.endpoints import HTTPEndpoint
from starlette.responses import PlainTextResponse
from starlette.routing import Route


class Driver(HTTPEndpoint):
    async def get(self, request):
        return PlainTextResponse(f'Hello, world!')


class User(HTTPEndpoint):

    async def get(self, request):
        username = request.path_params['username']
        return PlainTextResponse(f'Hello, {username}!')

    async def me(self, request):
        return PlainTextResponse(f'Hello, liang!')


routes = [
    Route('/', Driver),
    Route('/{username}', User)
]

app = Starlette(routes=routes)

if __name__ == '__main__':
    uvicorn.run(app=app)
