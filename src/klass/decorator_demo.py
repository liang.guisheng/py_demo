# A simple function wrapper demo for register an endpoint to API application
import types

application = {}
tracking = {}


# NOTE: helper method to register endpoint to API application object
def register(endpoint):
    if isinstance(endpoint, types.FunctionType):
        application[endpoint.__name__] = endpoint
    else:
        application[endpoint.__class__.__name__] = endpoint
    return endpoint


# NOTE: helper to mark public endpoint
def is_public(endpoint):
    endpoint.is_public = True
    return endpoint


def make_color(color):
    def color_decorator(endpoint):
        endpoint.color = color
        return endpoint

    return color_decorator


# def make_red(endpoint):  # change color of endpoint
#     endpoint.color = 'red'
#     return endpoint
#
#
# def make_blue(endpoint):  # change color of endpoint
#     endpoint.color = 'blue'
#     return endpoint

# NOTE: helper to tracking the invocation of endpoint
class invoke_tracer:
    def __init__(self, endpoint):
        self.endpoint = endpoint
        self.calls = 0

    def __call__(self, *args, **kwargs):
        self.calls += 1
        print(f'Calling {self.calls} to {self.endpoint.__name__} ')
        # NOTE: here we can do any interception tasks before invoke the real  function
        try:
            result = self.endpoint(*args, **kwargs)
        # NOTE: here we can do any interception tasks after invoke the real  function
        except Exception as e:
            return 'Error happen:' + str(e)
        else:
            return '已跟踪的结果:' + result


# NOTE: helper method to mock endpoint invocation
def request(name='', key=''):
    for api_name, api_obj in application.items():
        if api_name.startswith(name):
            if not key and not getattr(api_obj, 'is_public', False):
                raise Exception(f'Non-public api: {api_name} require key!')
            return api_obj()


# NOTE: helper method to test nesting decorator
def fetch_endpoint_by_color(color):
    return {api_name: api_object for api_name, api_object in application.items()
            if getattr(api_object, 'color', None) == color}


# NOTE: traditional way need extra helper invocation
def create_api():  # people/client does't know this is an application API endpoint directly
    return 'create object'


# FEATURE: use decorator to register endpoint to API application
# note: this is more explicit/concise
@register
def list_api():
    return 'list object'


@register
@is_public  # set a mark to object for later use
def detail_api():
    return 'detail object'


@register
@make_color('red')
def red_api():
    return 'red_api'


@register
@make_color('blue')
def blue_api():
    return 'blue api'


@register
@make_color('green')
def green_api():
    return 'green api'


@register
def purple_api():
    return 'purple api'


@register
@invoke_tracer
def tracer_api(obj=None):
    if isinstance(obj, Exception):
        raise obj
    return 'tracer_api'


#
if __name__ == '__main__':
    register(create_api)  # register endpoint to API application manually
    make_color('purple')(purple_api)

    for k, v in application.items():
        print(k, v)

    print(request('detail'))
    # request('list')
    print(fetch_endpoint_by_color('red'))
    print(fetch_endpoint_by_color('blue'))
    print(fetch_endpoint_by_color('green'))
    print(fetch_endpoint_by_color('purple'))

    print(tracer_api())
    print(tracer_api(Exception('test')))
