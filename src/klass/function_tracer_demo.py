from src.klass.python_overview import spam, spam2, spam3, spam4

if __name__ == '__main__':
    spam('a', 'b', 'c')
    spam('a', 'b', 'd')

    spam2('a', 'b', 'c')
    spam2('a', 'b', 'd')

    spam3('a', 'b', 'c')
    spam3('a', 'b', 'd')

    spam4('a', 'b', 'c')
    spam4('a', 'b', 'd')
