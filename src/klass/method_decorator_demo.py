from src.klass.python_overview import function_tracer_class, callable_tracer, descriptor_method_decorator_class, \
    simple_descriptor_method_decorator_class


class Person:

    def __init__(self, name, pay):
        self.name = name
        self.pay = pay

    # TODO: cause error
    @function_tracer_class
    def give_raise(self, percent):
        self.pay *= (1.0 + percent)

    # TODO: cause error
    @function_tracer_class
    def last_name(self):
        return self.name.split()[-1]

    @callable_tracer
    def full_name(self):
        return self.name

    @descriptor_method_decorator_class
    def descriptor_decorated_method1(self, name=None):  # triggered the descriptor protocol
        print(name or self.name)

    @simple_descriptor_method_decorator_class
    def descriptor_decorated_method2(self, name=None):
        print(name or self.name)


@descriptor_method_decorator_class
def descriptor_decorated_function1(a, b, c):
    print(a + b + c)


@simple_descriptor_method_decorator_class
def descriptor_decorated_function2(a, b, c):
    print(a + b + c)


if __name__ == '__main__':
    a = Person('alice', 1000)

    # NOTE: these 2 invocations will cause error
    # a.give_raise(0.1)
    # print(a.last_name)

    print(a.full_name)

    a.descriptor_decorated_method1()
    a.descriptor_decorated_method1('world')

    descriptor_decorated_function1(1, 2, 3)
    descriptor_decorated_function1(1, 2, 4)

    # nested wrapper inside decorator
    a.descriptor_decorated_method2('hello')
    a.descriptor_decorated_method2('world')

    descriptor_decorated_function2(1, 2, 3)
    descriptor_decorated_function2(1, 2, 4)
