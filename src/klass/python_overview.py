############################################################
# FEATURE: OOP designing with classes

# note:
#   1. OOP patterns
#       Inheritance:(is-a)
#       Composition:(has-a)
#       Delegation:(wrapper object)
############################################################


############################################################
# FEATURE: decorator
#
# note:
#   -------------------------
#   1. Definition:
#       decoration is way to specify management code for functions & classes
#       these code are inserted after the definition of function & class
#   -------------------------
#   2. Decorator types:
#       -------------------------
#       Function decorator:
#           plain function decoration
#           class method decoration
#       -------------------------
#       Class decorator:
#   -------------------------
#   3. Usages:
#       -------------------------
#       Function decorator usage:
#           1. Install wrapper objects to intercept later function calls and process them as needed
#           2. Manage function objects: register a function to an API
#       -------------------------
#       Class decorator usage:
#           1. Install wrapper objects to intercept later instance creation calls and process them as needed
#           2. Manage class object directly: augment a class with new method * same as metaclass*
#               (both class decorator and metaclass are at the end of class creation process)
#       -------------------------
#       Different between function decorator & class decorator
#           1. function decorator augment only a specific function&method call, not an entire object interface
#           2. class decorator(because they can intercept instance creation) can be used to implement arbitrary
#               object interface augmentation & management task.
#               ex:
#                   trace&validate every attribute reference made for an object
#                   implement proxy objects
#                   singleton classes
#                   other common coding patterns
#       -------------------------
#       Add call tracing, validity testing, thread lock access, timing profile
#       -------------------------
#       By returning the decorated object itself instead of a wrapper, decorator become a
#       simple post-creation step for functions&class
#   -------------------------
#   4. Why use decorator
#       Decorators provide a convenient and explicit way to code tools,
#       useful both during programming and in live production systems
#       ----------------------
#       4.1 Decorators have a very explicit syntax, which makes them easier to spot than helper
#           function calls
#       4.2 Decorators are applied once when the subject function or class is defined, no need to
#           add extra code at every call to the class or function
#       4.3 Because of both of the prior points, decorators make it less likely that a user of an
#           API will *forget* to augment a function or class according to API requirements
#       ----------------------
#       In other words, beyond their technical model, decorators offer some advantages in terms of
#       code maintenance and aesthetics(美学)
#       Moreover as structuring tools, decorators naturally foster encapsulation of code, which reduces
#       redundancy and makes future changes easier
#       --------Drawbacks--------------
#       Class decorator:
#           1. type changes
#           2. Extra calls
#       When they insert wrapper logic, they can alter the types of the decorated objects & they may
#       incur(招引) extra calls(which is drawback in any technique that adds wrapping logic to object).
#       --------Pros--------------
#           1. Explicit Syntax
#           2. Code maintenance
#           3. Consistency
#       ----------------------
#   -------------------------
#   5. how it works:
#       decorator achieve these effects by automatically rebinding function and class names
#       to other callable, at the end of def & class statement.
#       When later invoked, these callable can perform tasks such as tracing and timing function
#       calls, managing access to class instance attributes and so on
#   -------------------------
#   6. Implement decorator
#       6.1 Function decorator
#           Function decorators are largely just syntactic sugar that runs one function through another
#           at the end of a def statement, and rebinds the original function name to the result
import time


def simple_function_decorator(func_obj):
    # simple decorator to return the original function object directly
    # todo: do anything with this func_obj
    print('simple function decorator')
    return func_obj


def wrapper_function_decorator(func_obj):
    # decorator create a wrapper function object for the given func_obj and return
    # a new function object which inside will call the original func_obj
    print('wrapped function decorator')

    def wrapper(*args, **kwargs):
        # NOTE: we can do any interception tasks before function call
        result = func_obj(*args, **kwargs)  # call the real function object
        # NOTE:we can do any interception tasks after function call
        return result  # return result

    return wrapper  # return a new wrapper function object


class SimpleFunctionDecoratorClass:
    # decorator implemented in class instead of function form
    def __init__(self, func_obj):
        print('simple function decorator class!')
        self.func = func_obj

    def __call__(self, *args, **kwargs):
        # NOTE: we can do any interception tasks before function call
        result = self.func(*args, **kwargs)
        # NOTE: we can do any interception tasks after function call
        return result


# note
#       6.2 Class decorator
#           Class decorators are a way to manage class or wrap up instance construction calls with extra
#           logic that manages or augment instance created from a class
def simple_class_decorator(cls):
    # simple decorator to return the original cls object directly
    # todo: do any interception to the cls object
    return cls


def wrapper_class_decorator(cls):
    # decorator create a new wrapper class which will delegate original class's instance actions
    class WrappedClass:
        def __init__(self, *args, **kwargs):
            self.wrapped = cls(*args, **kwargs)

        def __getattr__(self, item):
            return getattr(self.wrapped, item)

    return WrappedClass


# note
#       6.3 Decorator nesting
#

# note
#       6.4 Decorator Arguments
#           Decorator with argument is like a decorator factory which generate another decorator with
#           given arguments

# note
#       6.5 Decorators Manage functions and class, too

# note
#   -------------------------
#   7. Decorator in practice(Good&drawback)
#       7.1 Coding Function decorators

class function_tracer_class:
    def __init__(self, func):
        self.calls = 0
        self.func = func

    def __call__(self, *args, **kwargs):
        self.calls += 1
        print(f'call {self.calls} to {self.func.__name__}')
        return self.func(*args, **kwargs)


function_calls = 0


def tracer_function_with_global(func):
    def wrapper(*args, **kwargs):
        global function_calls
        function_calls += 1
        print(f'call {function_calls} to {func.__name__}')
        return func(*args, **kwargs)

    return wrapper


def tracer_function_with_nonlocal(func):
    calls = 0

    def wrapper(*args, **kwargs):
        nonlocal calls
        calls += 1
        print(f'call {calls} to {func.__name__}')
        return func(*args, **kwargs)

    return wrapper


def tracer_function_with_func_attribute(func):
    def wrapper(*args, **kwargs):
        wrapper.calls += 1
        print(f'call {wrapper.calls} to {func.__name__}')
        return func(*args, **kwargs)

    wrapper.calls = 0
    return wrapper


@function_tracer_class
def spam(a, b, c):
    print(a + b + c)


@tracer_function_with_global
def spam2(a, b, c):
    print(a + b + c)


@tracer_function_with_nonlocal
def spam3(a, b, c):
    print(a + b + c)


@tracer_function_with_func_attribute
def spam4(a, b, c):
    print(a + b + c)


#       7.2 Coding Method decorators


def callable_tracer(callable):
    # This decorator works for both function and method
    calls = 0

    def on_call(*args, **kwargs):
        nonlocal calls
        calls += 1
        print(f'call {calls} to {calls}')
        return callable(*args, **kwargs)

    return on_call


class wrapper:
    def __init__(self, desc, subj):
        self.desc = desc
        self.subj = subj

    def __call__(self, *args, **kwargs):
        return self.desc(self.subj, *args, **kwargs)


class descriptor_method_decorator_class:
    def __init__(self, func):
        self.calls = 0
        self.func = func

    def __get__(self, instance, owner):
        print('calling descriptor protocol')
        return wrapper(self, instance)

    def __call__(self, *args, **kwargs):
        print('calling invocation')
        self.calls += 1
        print(f'call {self.calls} to {self.func.__name__}')
        return self.func(*args, **kwargs)


class simple_descriptor_method_decorator_class:
    def __init__(self, func):
        self.calls = 0
        self.func = func

    def __call__(self, *args, **kwargs):
        self.calls += 1
        print(f'call {self.calls} to {self.func.__name__}')
        return self.func(*args, **kwargs)

    def __get__(self, instance, owner):
        def wrapper(*args, **kwargs):
            return self(instance, *args, **kwargs)

        return wrapper


# note
#       7.3 Timing calls practice

class timer:
    def __init__(self, func):
        self.func = func
        self.all_time = 0

    def __call__(self, *args, **kwargs):
        start = time.clock()
        result = self.func(*args, **kwargs)
        elapsed = time.clock() - start
        self.all_time += elapsed
        print(f'{self.func.__name__}: {elapsed}, {self.all_time}')
        return result

    def __get__(self, instance, owner):
        def wrapper(*args, **kwargs):
            return self(instance, *args, **kwargs)

        return wrapper


def timer_with_label(label='', trace=True):
    class Timer:
        def __init__(self, func):
            self.func = func
            self.all_time = 0

        def __call__(self, *args, **kwargs):
            start = time.clock()
            result = self.func(*args, **kwargs)
            elapsed = time.clock() - start
            self.all_time += elapsed
            if trace:
                print(f'{label} {self.func.__name__}: {elapsed}, {self.all_time}')
            return result

        def __get__(self, instance, owner):
            def wrapper(*args, **kwargs):
                return self(instance, *args, **kwargs)

            return wrapper


# note
#       7.4 Class decorator practice
#           1. singleton
#           2. tracing object interface:(可以跟踪实例对象的所有动作)
#           3. decorator VS manager functions(不同的方法实现相同的作用)

# note
#       7.5 Managing functions and classes directly
#           Imagine that you require methods or classes used by an application to be registered to an
#           API for later processing.
#           --------------------------
#           Although you could provide a registration function to be called manually after the objects are
#           defined.
#           But a decorator make your intent more explicit
#           --------------------------
#

# note
#   8. Examples
#       8.1 "private" & "public" attributes
def accessControl(failIf):
    def onDecorator(aClass):
        class onInstance:
            def __init__(self, *args, **kwargs):
                self.__wrapped = aClass(*args, **kwargs)

            # note: new-style class can not intercet class's built-in methods invocation
            def __getattr__(self, item):
                if failIf(item):
                    raise TypeError(f'private attribute fetch:{item}')
                return getattr(self.__wrapped, item)

            def __setattr__(self, key, value):
                if key == '_onInstance__wrapped':
                    self.__dict__[key] = value

                elif failIf(key):
                    raise TypeError(f'private attribute change: {key}')

                else:
                    setattr(self.__wrapped, key, value)

        return onInstance

    return onDecorator


def Private(*attributes):
    return accessControl(failIf=(lambda attr: attr in attributes))


def Public(*attributes):
    return accessControl(failIf=(lambda attr: attr not in attributes))


def accessControl2(failIf):
    def onDecorator(aClass):
        def getatttributes(self, attr):
            if failIf(attr):
                raise TypeError(f'private attribute fetch: {attr}')
            else:
                return object.__getattribute__(self, attr)

        aClass.__getattribute__ = getatttributes
        return aClass

    return onDecorator


def Private2(*attributes):
    return accessControl(failIf=(lambda attr: attr in attributes))


def Public2(*attributes):
    return accessControl(failIf=(lambda attr: attr not in attributes))


############################################################


############################################################
# Python OOP
class Demo:
    ############################################################
    # Managed attribute:
    # NOTE: Inserting code to run on attribute access
    # Attribute interception technique
    # 1. fixme
    # 2. properties
    # 3. descriptors
    # 4. operator overload

    # NOTE: Descriptor protocol class
    class NameDescriptor:
        def __init__(self, name=None):
            self._name = name or 'default name'

        def __get__(self, instance, owner):
            return self._name

        def __set__(self, instance, value):
            self._name = value

        def __del__(self):
            del self._name

    name = NameDescriptor()

    def __getattr__(self, item):
        return super(Demo, self).__getattr__(item)

    def __setattr__(self, key, value):
        super(Demo, self).__setattr__(key, value)

    def __getattribute__(self, item):
        return super(Demo, self).__getattribute__(item)

    ############################################################
