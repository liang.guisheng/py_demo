class APIDescriptor:
    # a descriptor class implementation
    # TODO/NOTE: usage:
    #   introspection descriptor
    #   meta-descriptor

    def __init__(self, value=None):
        self._value = value

    def __set__(self, instance, value):
        print(f'setting descriptor attribute value:{value}')
        self._value = value

    def __get__(self, instance, klass):
        print(f'getting descriptor attribute value!')
        return self._value

    def __del__(self, instance):
        print(f'delete descriptor attribute value!')
        del self._value


def simple_class_decorator(cls):
    # a simple class decorator which only do a simple modification and return the original class
    # TODO/NOTE: Usage
    #   1. argument checking
    #   2. caching
    #   3. proxy
    #   4.context provider
    cls._class_decorator = 'simple_class_decorator'
    return cls


class Base:

    def __init__(self):
        print('Base __init__')

    def hello(self):
        return 'Hello, Base'


class BaseA(Base):

    def __init__(self):
        print('BaseA __init__')
        super(BaseA, self).__init__()

    # def hello(self):
    #     return 'Hello, BaseA'


class BaseB:

    def __init__(self):
        print('BaseB __init__')

    def hello(self):
        return 'Hello, BaseB'


@simple_class_decorator
class Demo(BaseA, BaseB):
    # descriptor protocol
    value = APIDescriptor('demo')

    # fake private attribute by convention
    _fake_private_attr = 'fake private value'

    # not really private, but changed name during interpreter compilation
    __some_real_private_attr = 'some real private value'

    ##################################################

    # TODO: has conflict with descriptor attribute
    __slots__ = ['name', '_with_context', '_iter_init', 'index_slice_obj']

    def __init__(self, name=None, iter_init=10):
        # init value for iteration protocol
        self.name = name
        self._iter_init = iter_init
        self._with_context = [1, 2, 3]
        self.index_slice_obj = [i for i in range(10)]
        super(Demo, self).__init__()

    @classmethod
    def klass_method(cls):
        return 'Class method can be called by class directly'

    @staticmethod
    def static_method():
        return 'static method does not rely on any other class/instance context'

    ##################################################
    # TODO/NOTE: Attribute reference:
    #   1. __getattr__
    #   2. __setattr__

    def __getattr__(self, item):
        if item not in self.__slots__:
            print(f'built-in __get_attr__() calling with {item}')
        return super().__getattr__(item)

    def __setattr__(self, key, value):
        if key not in self.__slots__:
            print(f'built-in set attribute calling: {key} - {value}')
        super(Demo, self).__setattr__(key, value)

    def __getattribute__(self, item):
        # intercept instance's non exist attribute access
        if item == 'non_exist_value':
            print(f'calling __getattribute__() method with {item}!')
        return super(Demo, self).__getattribute__(item)

    ##################################################
    # iterator protocol: iterator object
    def __next__(self):
        if self._iter_init == 0:
            print('exiting iterator context!!!')
            raise StopIteration()
        self._iter_init -= 1
        return self._iter_init

    def __iter__(self):
        print('entering iterator protocol context')
        return self

    ##################################################

    ##################################################
    # context manager protocol
    def __enter__(self):
        print('entering with manager context!')
        return self._with_context[:]

    def __exit__(self, exc_type, exc_value, exc_tb):
        if not exc_type:
            print('exiting with manager context normally!')
        else:
            print(f'exiting with manager context error {exc_type}!')

    ##################################################

    ##################################################
    # indexing & slice
    def __getitem__(self, index):
        return self.index_slice_obj[index]

    def __setitem__(self, key, value):
        self.index_slice_obj[key] = value

    ##################################################

    ##################################################
    # TODO/NOTE: Membership test:
    #   1. __contains__
    #   2. __iter__
    #   3. __getitem__

    ##################################################

    ##################################################
    # TODO/NOTE: String representation
    #   1. __repr__
    #   2. __str__
    def __str__(self):
        return f'Demo Class: {self.name}'

    ##################################################

    ##################################################
    # TODO/NOTE: Boolean test
    #   1. __bool__
    #   2. __len__
    def __bool__(self):
        return True

    def __len__(self):
        return 0

    ##################################################

    ##################################################
    # TODO/NOTE: Call expression
    #   1. __call__
    def __call__(self, *args, **kwargs):
        print(f'Instance of Demo class: {self.name} has been called!')
    ##################################################


if __name__ == '__main__':
    d = Demo(iter_init=20)
    d1 = Demo(iter_init=30)

    # demo class's inheritance
    print(d.hello())

    # demo class's fake private attribute
    print(d._fake_private_attr)

    # demo class's some real private attribute
    # print(d.__some_real_private_attr)  # TODO: cause exception

    # demo class's some real private attribute's right way to access
    # print(d._Demo__some_real_private_attr)

    print(d.value)
    d.value = 'descriptor value'
    print(d.value)

    d.non_exist_value = 'non exit value'
    print(d.__dict__)
    print(d.non_exist_value)

    print(d.klass_method())
    print(d1.klass_method())

    print(d.static_method())

    # class level decorator
    print(d._class_decorator)

    # Iterator protocol context
    print([i for i in d])

    with Demo() as o:
        print(o)

    # d.value = 'descriptor'
    # print(d.descriptor_attr)

    # index & slice overload
    print(d[0])
    d[0] = 11
    print(d[0])
