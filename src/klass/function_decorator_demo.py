from src.klass.python_overview import simple_function_decorator, wrapper_function_decorator, \
    SimpleFunctionDecoratorClass


@simple_function_decorator
def simple_function1(name):
    print(name)


@wrapper_function_decorator
def simple_function2(name):
    print(name)


@SimpleFunctionDecoratorClass
def simple_function3(name):
    print(name)


@SimpleFunctionDecoratorClass
@wrapper_function_decorator
@simple_function_decorator
def simple_function4(name):
    print(name)


if __name__ == '__main__':
    print(simple_function1.__name__)
    print(simple_function2.__name__)
    print(simple_function3.__class__.__name__)
