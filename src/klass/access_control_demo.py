from src.klass.python_overview import Private


@Private('age')
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age
    #
    # def __str__(self):
    #     return f'Person: {self.name}  - {self.age}'
    #
    # def __add__(self, yrs):
    #     self.age += yrs

    # def __iadd__(self, yrs):
    #     self.__add__(yrs)


if __name__ == '__main__':
    x = Person('Bob', 40)
    print(x.name)
    print(x)
