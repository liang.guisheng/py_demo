from src.klass.python_overview import wrapper_class_decorator, simple_class_decorator


@simple_class_decorator
class SimpleClass:
    def __init__(self, name):
        self.name = name


@wrapper_class_decorator
class SimpleClass2:

    def __init__(self, name=None):
        self.name = name



if __name__ == '__main__':
    print(SimpleClass.__name__)
    print(SimpleClass2.__name__)
    print('\n')
    a = SimpleClass2('liang')
    b = SimpleClass2('yara')
    print(a.name)
    print(b.name)
