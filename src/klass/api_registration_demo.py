registry = {}


def register(obj):
    registry[obj.__name__] = obj
    return obj


def is_public(obj):
    """ set a is_public mark to obj """
    obj.is_public = True
    return obj


def request(name=None, key=None):
    for api_name, api in registry.items():
        if api_name.startswith(name):
            if not key and not getattr(api, 'is_public', False):
                raise Exception(f'Non public api: {api_name} call require key')
            return api()


############################################################
# FEATURE: use decorator
@is_public
@register
def list_api():
    return 'list api'


@register
def create_api():
    return 'create api'


@register
def update_api():
    return 'update api'


############################################################
# FEATURE: use callback
def patch_api():
    return 'patch api'


patch_api = register(patch_api)


def delete_api():
    return 'delete api'


delete_api = register(delete_api)
############################################################

if __name__ == '__main__':
    print(request('list'))
    print(request('create'))
