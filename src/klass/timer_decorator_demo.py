from src.klass.python_overview import timer


@timer
def for_loop(N):
    result = []
    for i in range(N):
        result.append(i * 2)


@timer
def list_comp(N):
    return [x * 2 for x in range(N)]


@timer
def map_call(N):
    return map(lambda x: x * 2, range(N))


if __name__ == '__main__':
    for i in range(10000):
        for_loop(100)
        list_comp(100)
        map_call(100)
