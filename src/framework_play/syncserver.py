import re
from collections import MutableMapping
from typing import Callable, List


class MultiDict(MutableMapping):
    pass


class BaseRequest:
    """ Used to handle http request """
    pass


class BaseResponse:
    """ Used to handle http response """
    pass


class Application:
    pass


def flatten_re(pattern):
    if '(' not in pattern: return pattern
    return re.sub(r'(\\*)(\?P<[^>]>/\((?!\?))', lambda m: m.group(0) if len(m.group(1)) % 2 else m.group(1) + '(?:', p)


class Router:
    __slots__ = [
        'paths', 'groups', 'builder', 'static', 'dyna_routes', 'dyna_regexes', 'strict_order', 'filters'
    ]

    default_pattern = '[^/]+'

    def __init__(self, strict: bool = False):
        self.paths = []
        self.groups = {}
        self.builder = {}  # data structure for url builder
        self.static = {}  # static routes
        self.dyna_routes = {}
        self.dyna_regexes = {}
        self.strict_order = strict
        self.filters = {
            're': lambda conf: (),
            'int': lambda conf: (r'-?\d+', int, lambda x: str(int(x))),
            'float': lambda conf: (r'-?[\d.]+', float, lambda x: str(float(x))),
            'path': lambda conf: (r'.+?', None, None)
        }

    def add_filter(self, name, func: Callable):
        self.filters[name] = func

    def add(self, path, method, callback, name=None):
        pass


class Route:
    __slots__ = [
        'app', 'path', 'method', 'callback', 'name', 'plugins', 'skiplist', 'extra'
    ]

    def __init__(self, app: Application, path: str, method: str, callback: Callable, name: str, plugins: List = None,
                 skiplist: List = None, **extra):
        self.app = app
        self.path = path
        self.method = method
        self.callback = callback
        self.name = name
        self.plugins = plugins or []
        self.skiplist = skiplist or []
