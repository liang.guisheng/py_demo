def gen_error():
    try:
        result = 1 / 0
        print('return result!')
        return result
    except Exception as e:
        print('catching error!')
        raise e


if __name__ == '__main__':
    result = gen_error()
    print(result)
