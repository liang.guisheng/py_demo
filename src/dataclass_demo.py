#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/10/19 9:47 AM
@Author: liang
@File: dataclass_demo.py
"""
from dataclasses import dataclass, field


@dataclass
class InventoryItem:
    name: str
    unit_price: float
    quantity_on_hand: int = 0
    total_cost: float = field(init=False)

    def __post_init__(self):
        self.total_cost = self.unit_price * self.quantity_on_hand
