#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Creation: 9/14/19 8:33 PM
@Author: liang
@File: ws_client_demo.py
"""
from ws4py.client.threadedclient import WebSocketClient


class DummyClient(WebSocketClient):
    def opened(self):
        def data_provider():
            for i in range(1, 200, 25):
                yield '#' * i

        self.send(data_provider())

        for i in range(0, 200, 25):
            print(i)
            self.send('*' * i)

    def closed(self, code, reason=None):
        print('Closed down', code, reason)

    def received_message(self, message):
        print(f'Received message: {message}\n')
        if len(message) == 175:
            self.close(reason='Bye bye')


if __name__ == '__main__':
    try:
        ws = DummyClient('ws://localhost:9000/', protocols=['http-only', 'chat'])
        ws.connect()
        ws.run_forever()
    except KeyboardInterrupt:
        ws.close()
