def lengthOfLongestSubstring(s: str) -> int:
    all_sets = set()
    current_set = []
    for c in s:
        if c not in current_set:
            current_set.append(c)
            continue

        index_c = current_set.index(c)
        if index_c != 0:
            continue

        reject_sub_set = ''.join(current_set[0:index_c + 1])

        if reject_sub_set not in all_sets:
            all_sets.add(reject_sub_set)
        current_set = current_set[index_c + 1:]
        current_set.append(c)

    all_sets.add(''.join(current_set))

    print(all_sets)


if __name__ == '__main__':
    lengthOfLongestSubstring('pwwkew')
