class Solution:
    def isMatch(self, s: str, p: str) -> bool:
        print(f'{s}-{p}')
        if not p and s:
            return False
        markers = self.split_pattern(p)

        current_step = 0
        for marker in markers:
            if current_step >= len(s):
                return '*' in marker[1] and marker[3] == 0
            for i in range(current_step, len(s)):
                current_char = s[i]
                if '*' not in marker[1]:
                    if marker[1] == '.' or marker[1] == current_char:
                        current_step += 1
                        break
                    else:
                        return False

                if (marker[1].startswith('.') or marker[1].startswith(current_char)) and len(s[i:]) == marker[3] and \
                        marker[3]:
                    break

                if not marker[1].startswith('.') and not marker[1].startswith(current_char):
                    break

                current_step += 1
                break

        return current_step >= len(s)

    def split_pattern(self, p: str):
        markers = []
        current_marker_range = 0
        for i, c in enumerate(p):
            if i < current_marker_range:
                continue
            next = i + 1
            if next < len(p):
                if p[next] == '*':
                    markers.append([i, c + p[next]])
                elif c == '*':
                    continue
                else:
                    markers.append([i, c])
            else:
                if c != '*':
                    markers.append([i, c])
        for i, marker in enumerate(markers):
            if '*' in marker[1]:
                marker.append(len([m for m in markers[0:i] if '*' not in m[1]]))
                marker.append(len([m for m in markers[i + 1:] if '*' not in m[1]]))

        print(markers)
        return markers


if __name__ == '__main__':
    for e in [
        # ('a', 'ab*'),
        # ('aa', 'a'),
        # ('ab', '.*..'),
        # ('aa', 'a*'),
        # ('aa', 'a*a'),
        # ('aa', 'aaa'),
        # ('ab', '.*'),
        # ('ab', '.*c'),
        # ('aab', 'c*a*b'),
        # ('mississippi', 'mis*is*p*.'),
        # ('aaa', 'ab*a*c*a'),
        ("aasdfasdfasdfasdfas", "aasdf.*asdf.*asdf.*asdf.*s")
    ]:
        print(Solution().isMatch(*e))
