class Solution:
    def convert(self, s: str, numRows: int) -> str:
        length = len(s)
        if length < numRows:
            return s

        new_s = ''
        for i in range(numRows):
            if i == 0 or i == numRows - 1:
                index = i
                while True:
                    if index < length:
                        new_s += s[index]
                        if numRows > 1:
                            index += (2 * numRows - 2)
                        else:
                            index += 1
                    else:
                        break
            else:
                index = i
                while True:
                    if index < length:
                        new_s += s[index]

                        index += 2 * (numRows - 1 - i)
                        if index < length:
                            new_s += s[index]
                        index += 2 * i
                    else:
                        break
        return new_s


if __name__ == '__main__':
    for s in [
        # ('PAYPALISHIRING', 3), ('LEETCODEISHIRING', 4),
        ('AB', 1)]:
        print(Solution().convert(*s))
