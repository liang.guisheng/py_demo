class Solution:
    def isPalindrome(self, x: int) -> bool:
        origin_seq, reversed_seq = [], []
        if x < 0:
            return False

        while True:
            digit = x % 10
            reversed_seq.append(digit)
            origin_seq.insert(0, digit)
            x //= 10
            if not x:
                break

        return origin_seq == reversed_seq


if __name__ == '__main__':
    for n in [121, -121, 234]:
        Solution().isPalindrome(n)
