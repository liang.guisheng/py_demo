class Solution:
    def reverse(self, x: int) -> int:
        negative = x < 0
        if negative:
            x = -x
        reversed_num = []
        while True:
            last_digit = x % 10

            left_value = x // 10

            if left_value == 0:
                reversed_num.append(last_digit)
                break
            reversed_num.append(last_digit)
            x = left_value

        pre_zero = True
        for i, n in enumerate(reversed_num[:]):
            if n == 0 and pre_zero:
                reversed_num[i] = None
                continue
            pre_zero = False

        reversed_num = [e for e in reversed_num if e is not None]
        print(reversed_num)
        result = sum([v * 10 ** i for i, v in enumerate(reversed(reversed_num))])

        result = -result if negative else result

        if -(1 << 31) <= result <= (1 << 31) - 1:
            return result
        return 0


if __name__ == '__main__':
    for n in [12300, 1009, 65400]:
        Solution().reverse(n)
