from typing import List
def twoSum(nums: List[int], target: int) -> List[int]:
    
    base = {}
    for i,v in enumerate(nums):
        base.setdefault(v,[]).append(i)
        
    print(base)
    for v in sorted(base.keys()):
        
        rest = target - v
        if rest ==v and len(base[rest])>1:
            return base[rest][0:2]
        if rest in base:
            print(v, rest, base[v],base[rest])
            return [base[v][0],base[rest][0]]
        

def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        index = 0
        head=result = None
        offset = 0
        while l1 or l2:
            v = (l1.val or 0) + (l2.val or 0) +offset
            l1=l1.next
            l2=l2.next
            cv = v % 10
            offset = int(cv/10)
            if index ==0:
                head = ListNode(cv)
                index +=1
                result = head.next
                continue
            result=ListNode(cv)
            result = result.next
            
            

if __name__=='__main__':
    case  = [3,2,3]
    target = 6
    print(twoSum(case, target))
    assert twoSum(case, target) == [0, 2]