def myAtoi(s: str) -> int:
    valid_num = []
    negative = False
    strip_value = s.strip()
    if not strip_value:
        return 0

    if not strip_value[0].isdigit():
        if strip_value[0] not in ['+', '-']:
            return 0
        negative = True if strip_value[0] == '-' else False
        strip_value = strip_value[1:]

    for i, c in enumerate(strip_value):

        if not c.isdigit():
            break

        valid_num.append(int(c))

    if not valid_num:
        return 0

    valid_num = [v * 10 ** i for i, v in enumerate(reversed(valid_num))]
    result = sum(valid_num)
    if negative:
        if result > 1 << 31:
            return -(1 << 31)
        return -result
    if (result + 1) > 1 << 31:
        return (1 << 31) - 1
    return result


if __name__ == '__main__':
    for s in ['42', '   -42', '4198 with words', 'words and 987', '-91283472332', "2147483646", "2147483648"]:
        print(myAtoi(s))
