        
        
def check_padlindrome(s:str)->bool:
    for i in range(int(len(s)/2)):
        if s[i] != s[0-i-1]:
            return False
    return True


if __name__ == "__main__":
    print(check_padlindrome('a'))
    print(check_padlindrome('aa'))
    print(check_padlindrome('ab'))
    print(check_padlindrome('abc'))
    print(check_padlindrome('aba'))